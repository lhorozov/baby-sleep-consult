import React from 'react';
import './ComingSoon.css';
import comingsoon from '../images/coming-soon.jpeg';

export const ComingSoon = () => {
  return (
    <div className="coming-soon-logo-container">
      <img className="coming-soon-logo" src={comingsoon} alt="Coming soon"></img>
    </div>
  );
};
