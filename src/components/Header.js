import React, { useState } from 'react';
import { NavLink, useNavigate } from 'react-router-dom';

import logo from '../images/logo-horizontal-cut.png';
import './Header.css';

const Header = () => {
  const navNames = [
    { show: 'Начало', to: '/' },
    { show: 'За мен', to: 'about' },
    { show: 'Услуги', to: 'services' },
    { show: 'FAQ', to: 'faq' },
    { show: 'Контакти', to: 'contacts' },
    { show: 'Магазин', to: 'shop' },
  ];

  const [showMenu, setShowMenu] = useState(false);
  const navigate = useNavigate();

  const listItems = navNames.map((name) => (
    <li className="header-li" key={name.show} onClick={() => setShowMenu(false)}>
      <NavLink className="header-li-link" to={name.to}>
        {name.show}
      </NavLink>
    </li>
  ));

  const imageClickHandler = () => {
    navigate('/');
  };

  const menuClickHandler = () => {
    setShowMenu(!showMenu);
  };

  return (
    <header className="header">
      <img className="header-img" src={logo} alt="Logo" onClick={imageClickHandler} />
      <nav className="header-nav">
        <ul className="header-ul">{listItems}</ul>
      </nav>
      <nav className="header-nav__menu-icon" onClick={menuClickHandler}>
        {showMenu ? null : <i className="fas fa-bars fa-2x"></i>}
      </nav>
      {showMenu && (
        <div className="header-nav__menu">
          <ul className="header-ul__menu">{listItems}</ul>
        </div>
      )}
    </header>
  );
};

export default Header;
