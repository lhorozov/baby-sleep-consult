import React from 'react';
import { ComingSoon } from '../components/ComingSoon';
import './AboutMe.css';

const AboutMe = () => {
  return (
    <div className="about-me">
      <ComingSoon />
    </div>
  );
};

export default AboutMe;
