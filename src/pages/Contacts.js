import React from 'react';
import { ComingSoon } from '../components/ComingSoon';
import './Contacts.css';

const Contacts = () => {
  return (
    <div className="contacts">
      <ComingSoon />
    </div>
  );
};

export default Contacts;
