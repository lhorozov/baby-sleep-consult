import React from 'react';
import { ComingSoon } from '../components/ComingSoon';
import './FAQ.css';

const FAQ = () => {
  return <div className="faq"><ComingSoon /></div>;
};

export default FAQ;
