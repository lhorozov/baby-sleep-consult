import React from 'react';
import './Home.css';
import background from '../images/background-blue-eyes-baby.jpg';

const Home = () => {
  return (
    <main className="home">
      <aside className="home-aside">
        <section className="home-social">
          <ul className="home-social-ul">
            <li className="home-social-li">
              <a href="https://www.facebook.com/petyahorozovaconsult">
                <i className="fab fa-facebook-square fa-2x"></i>
              </a>
            </li>
            <li className="home-social-li">
              <a href="https://www.instagram.com/petyahorozova/">
                <i className="fab fa-instagram-square fa-2x"></i>
              </a>
            </li>
          </ul>
        </section>
      </aside>
      <img className="home-background" src={background} alt="Background"></img>
      <div className="home-slogan-wrapper">
        <p className="home-slogan noselect">
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Doloribus officia ex reprehenderit enim magni natus
          mollitia asperiores aperiam perspiciatis assumenda, odit sequi eos neque praesentium provident exercitationem
          illum voluptatum atque.
        </p>
      </div>
    </main>
  );
};

export default Home;
