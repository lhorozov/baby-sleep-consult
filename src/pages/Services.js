import React from 'react';
import { ComingSoon } from '../components/ComingSoon';
import './Services.css';

const Services = () => {
  return (
    <div className="services">
      <ComingSoon />
    </div>
  );
};

export default Services;
