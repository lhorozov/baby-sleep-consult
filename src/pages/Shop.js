import React, { useState } from 'react';
import { ComingSoon } from '../components/ComingSoon';
import './Shop.css';

const FormSubmission = () => {
  return (
    <div className="shop-container">
      <form className="shop-form" target="_blank" action="https://formsubmit.co/ps.horozova@gmail.com" method="POST">
        <h2 className="shop-form-label">Данни за поръчка</h2>
        <input className="shop-form-input" type="text" name="name" placeholder="Име и фамилия" required />
        <input className="shop-form-input" type="text" name="phone" placeholder="Телефон за контакт" required />
        <textarea
          className="shop-form-input"
          placeholder="Адрес за доставка"
          name="address"
          rows="10"
          required
        ></textarea>
        <button className="shop-submit-button" type="submit">
          Направи поръчка
        </button>
      </form>
    </div>
  );
};

const Shop = () => {
  const [isPageReady] = useState(false);

  return isPageReady ? (
    <div classNameName="shop">
      <FormSubmission />
    </div>
  ) : (
    <ComingSoon />
  );
};

export default Shop;
